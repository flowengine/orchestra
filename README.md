# FlowEngine Orchestra #

FlowEngine Orchestra is part of the FlowEngine project. Orchestra is used to organize a series of steps to be executed on a particular target. What we do is set up our Orchestrator with the desired Actors for a Class:


```
#!java

Orchestrator orchestrator = new Orchestrator();
orchestrator.registerActorForClass(TargetObject.class, new SimpleTestActor(1));
orchestrator.registerActorForClass(TargetObject.class, new SimpleTestActor2(2));
orchestrator.buildExecutionTrees();
TargetObject testTarget = new TargetObject();
orchestrator.execute(testTarget);
```

The idea is that after the execution trees are built, any call to the orchestrator given a class it will invoke the corresponding Actors in order. This is very useful to integrate different modules that may depend on each other. This will be used with FlowEngine Injector to bind components together, providing a way to integrate custom components.

See [https://packagecloud.io/flowengine/orchestra](Link URL) for installation steps