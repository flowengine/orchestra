package TestActors;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ignacio on 20/08/2016.
 */
public class TargetObject {
    private List<Integer> elements;

    public TargetObject() {
        elements = new ArrayList();
    }

    public void addElement(Integer i) {
        elements.add(i);
    }

    public List<Integer> getElements() {
        return elements;
    }


    public boolean comesBefore(Integer before, Integer latter) {
        for (Integer value : getElements()) {
            if (value == before) {
                return true;
            }
            if (value == latter) {
                return false;
            }
        }
        return false;
    }
}
