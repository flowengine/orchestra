package TestActors;

import flowengine.orchestra.orchestrator.Actor;

/**
 * Created by Ignacio on 20/08/2016.
 */
public class SimpleTestActor2 implements Actor<TargetObject> {
    Integer order;

    public SimpleTestActor2(Integer order) {
        this.order = order;
    }

    public void execute(TargetObject target) {
        target.addElement(order);
    }
}
