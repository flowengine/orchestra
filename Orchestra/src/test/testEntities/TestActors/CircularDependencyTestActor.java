package TestActors;

import flowengine.orchestra.annotations.DependsOn;
import flowengine.orchestra.orchestrator.Actor;

/**
 * Created by Ignacio on 20/08/2016.
 */
@DependsOn(actors = {CircularDependencyTestActor2.class})
public class CircularDependencyTestActor implements Actor<TargetObject> {
    Integer order;

    public CircularDependencyTestActor(Integer order) {
        this.order = order;
    }

    public void execute(TargetObject target) {
        target.addElement(order);
    }
}
