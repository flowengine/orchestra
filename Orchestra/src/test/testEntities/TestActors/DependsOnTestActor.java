package TestActors;

import flowengine.orchestra.annotations.DependsOn;
import flowengine.orchestra.orchestrator.Actor;

/**
 * Created by Ignacio on 20/08/2016.
 */
@DependsOn(actors = {SimpleTestActor.class, SimpleTestActor2.class})
public class DependsOnTestActor implements Actor<TargetObject> {
    Integer order;

    public DependsOnTestActor(Integer order) {
        this.order = order;
    }

    public void execute(TargetObject target) {
        target.addElement(order);
    }
}
