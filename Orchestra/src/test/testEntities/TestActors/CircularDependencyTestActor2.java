package TestActors;

import flowengine.orchestra.annotations.DependsOn;
import flowengine.orchestra.orchestrator.Actor;

/**
 * Created by Ignacio on 20/08/2016.
 */
@DependsOn(actors = {CircularDependencyTestActor.class})
public class CircularDependencyTestActor2 implements Actor<TargetObject> {
    Integer order;

    public CircularDependencyTestActor2(Integer order) {
        this.order = order;
    }

    public void execute(TargetObject target) {
        target.addElement(order);
    }
}
