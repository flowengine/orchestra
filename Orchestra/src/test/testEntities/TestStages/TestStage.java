package TestStages;

import TestActors.TargetObject;
import flowengine.orchestra.annotations.Phase;
import flowengine.orchestra.annotations.SetOrchestrator;
import flowengine.orchestra.annotations.Stage;
import flowengine.orchestra.orchestrator.Orchestrator;

/**
 * Created by Ignacio on 21/08/2016.
 */
@Stage(forClass = TargetObject.class)
public interface TestStage {

    @SetOrchestrator(phase = "create")
    void setOrchestratorCreate(Orchestrator orchestrator);

    @SetOrchestrator(phase = "stop")
    void setOrchestratorStop(Orchestrator orchestrator);

    @Phase(name = "create")
    void executeCreate(TargetObject targetObject);

    @Phase(name = "stop")
    void executeStop(TargetObject targetObject);
}
