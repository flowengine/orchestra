package flowengine.orchestra.orchestrator;

import TestActors.*;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Ignacio on 20/08/2016.
 */
public class ExecutionPlanTest extends TestCase {

    public ExecutionPlanTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(ExecutionPlanTest.class);
    }

    public void testSingleActorExecutionPlan() {
        Set<Actor> actors = new HashSet();
        actors.add(new SimpleTestActor(1));
        ExecutionPlan executionPlan = new ExecutionPlan(actors);
        TargetObject testTarget = new TargetObject();
        executionPlan.execute(testTarget);
        assertEquals(1, testTarget.getElements().size());
        assertEquals(new Integer(1), testTarget.getElements().get(0));
    }

    public void testTwoSimpleActorExecutionPlan() {
        Set<Actor> actors = new HashSet();
        actors.add(new SimpleTestActor(1));
        actors.add(new SimpleTestActor2(2));
        ExecutionPlan executionPlan = new ExecutionPlan(actors);
        TargetObject testTarget = new TargetObject();
        executionPlan.execute(testTarget);
        assertEquals(2, testTarget.getElements().size());
        assertTrue(testTarget.getElements().contains(new Integer(1)));
        assertTrue(testTarget.getElements().contains(new Integer(2)));
    }

    public void testExecutionPlanWithTwoSimpleActorWithActorThatDependsFromThem() {
        Set<Actor> actors = new HashSet();
        actors.add(new SimpleTestActor(1));
        actors.add(new SimpleTestActor2(2));
        actors.add(new DependsOnTestActor(3));
        ExecutionPlan executionPlan = new ExecutionPlan(actors);
        TargetObject testTarget = new TargetObject();
        executionPlan.execute(testTarget);
        assertEquals(3, testTarget.getElements().size());
        assertTrue(testTarget.getElements().contains(new Integer(1)));
        assertTrue(testTarget.getElements().contains(new Integer(2)));
        assertTrue(testTarget.getElements().contains(new Integer(3)));
        assertTrue(testTarget.comesBefore(1, 3));
        assertTrue(testTarget.comesBefore(2, 3));
    }

    @org.junit.Test(expected = RuntimeException.class)
    public void testExecutionWithFirstDegreeCircularDependency() {
        Set<Actor> actors = new HashSet();
        actors.add(new CircularDependencyTestActor(1));
        actors.add(new CircularDependencyTestActor2(2));
        ExecutionPlan executionPlan = new ExecutionPlan(actors);
    }

    @org.junit.Test(expected = RuntimeException.class)
    public void testExecutionWithUnresolvedDependencies() {
        Set<Actor> actors = new HashSet();
        actors.add(new DependsOnTestActor(1));
        ExecutionPlan executionPlan = new ExecutionPlan(actors);
    }
}