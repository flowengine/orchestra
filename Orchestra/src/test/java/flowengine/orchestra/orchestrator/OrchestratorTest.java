package flowengine.orchestra.orchestrator;

import TestActors.DependsOnTestActor;
import TestActors.SimpleTestActor;
import TestActors.SimpleTestActor2;
import TestActors.TargetObject;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by Ignacio on 20/08/2016.
 */
public class OrchestratorTest extends TestCase {

    public OrchestratorTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(OrchestratorTest.class);
    }

    public void testSingleActorOrchestra() {
        Orchestrator orchestrator = new Orchestrator();
        orchestrator.registerActorForClass(TargetObject.class, new SimpleTestActor(1));
        orchestrator.buildExecutionTrees();
        TargetObject testTarget = new TargetObject();
        orchestrator.execute(testTarget);
        assertEquals(1, testTarget.getElements().size());
        assertEquals(new Integer(1), testTarget.getElements().get(0));
    }

    public void testTwoSimpleActorsOrchestra() {
        Orchestrator orchestrator = new Orchestrator();
        orchestrator.registerActorForClass(TargetObject.class, new SimpleTestActor(1));
        orchestrator.registerActorForClass(TargetObject.class, new SimpleTestActor2(2));
        orchestrator.buildExecutionTrees();
        TargetObject testTarget = new TargetObject();
        orchestrator.execute(testTarget);
        assertEquals(2, testTarget.getElements().size());
        assertTrue(testTarget.getElements().contains(new Integer(1)));
        assertTrue(testTarget.getElements().contains(new Integer(2)));
    }

    public void testOrchestraWithTwoSimpleActorWithActorThatDependsFromThem() {
        Orchestrator orchestrator = new Orchestrator();
        orchestrator.registerActorForClass(TargetObject.class, new SimpleTestActor(1));
        orchestrator.registerActorForClass(TargetObject.class, new SimpleTestActor2(2));
        orchestrator.registerActorForClass(TargetObject.class, new DependsOnTestActor(3));
        orchestrator.buildExecutionTrees();
        TargetObject testTarget = new TargetObject();
        orchestrator.execute(testTarget);
        assertEquals(3, testTarget.getElements().size());
        assertTrue(testTarget.getElements().contains(new Integer(1)));
        assertTrue(testTarget.getElements().contains(new Integer(2)));
        assertTrue(testTarget.getElements().contains(new Integer(3)));

        assertTrue(testTarget.comesBefore(1, 3));
        assertTrue(testTarget.comesBefore(2, 3));
    }
}