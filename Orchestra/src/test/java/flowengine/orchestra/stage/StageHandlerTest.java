package flowengine.orchestra.stage;

import TestActors.SimpleTestActor;
import TestActors.TargetObject;
import TestStages.TestStage;
import flowengine.orchestra.orchestrator.Orchestrator;
import junit.framework.TestCase;

/**
 * Created by Ignacio on 22/08/2016.
 */
public class StageHandlerTest extends TestCase {

    private StageHandler stageHandler;

    private StageFactory stageFactory;

    private StageActionFactory stageActionFactory;

    public void setUp() throws Exception {
        super.setUp();
        stageActionFactory = new StageActionFactoryImplementor();
        stageFactory = new StageFactoryImplementor(stageActionFactory);
        stageHandler = new StageHandler(new StageActionFactoryImplementor(), TestStage.class, "");
    }

    public void testExecutePhase() throws Exception {
        TargetObject obj = new TargetObject();
        Orchestrator o1 = createOrchestrator(1);
        Orchestrator o2 = createOrchestrator(2);
        stageHandler.setOrchestratorForPhase("create", o1);
        stageHandler.setOrchestratorForPhase("stop", o2);
        TestStage stage = getTestStage();
        stageHandler.executePhase("create", obj, stage);
        assertEquals(1, obj.getElements().size());
        assertEquals(new Integer(1), obj.getElements().get(0));
        stageHandler.executePhase("stop", obj, stage);
        assertEquals(2, obj.getElements().size());
        assertEquals(new Integer(2), obj.getElements().get(1));
        assertTrue("2 comes after 1", obj.comesBefore(1, 2));
    }

    private TestStage getTestStage() {
        return stageFactory.getStage(TestStage.class);
    }

    private Orchestrator createOrchestrator(Integer i) {
        Orchestrator o1 = new Orchestrator();
        o1.registerActorForClass(TargetObject.class, new SimpleTestActor(i));
        o1.buildExecutionTrees();
        return o1;
    }
}