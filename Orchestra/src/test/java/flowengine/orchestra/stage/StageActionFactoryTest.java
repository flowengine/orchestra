package flowengine.orchestra.stage;

import TestActors.TargetObject;
import TestStages.TestStage;
import flowengine.orchestra.orchestrator.Orchestrator;
import flowengine.orchestra.stage.actions.Action;
import flowengine.orchestra.stage.actions.PhaseAction;
import flowengine.orchestra.stage.actions.SetOrchestratorAction;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

/**
 * Created by Ignacio on 20/08/2016.
 */
public class StageActionFactoryTest extends TestCase {

    public StageActionFactoryTest(String testName) {
        super(testName);
    }

    public static Test suite() {
        return new TestSuite(StageActionFactoryTest.class);
    }

    private StageActionFactory factory;

    public void setUp() {
        factory = new StageActionFactoryImplementor();
    }

    public void testShouldReturnSetOrchestratorHandler() {
        Action returnAction = null;
        try {
            returnAction = factory.getAction(TestStage.class.getMethod("setOrchestratorCreate", Orchestrator.class));
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e.getMessage());
        }
        assertTrue(returnAction instanceof SetOrchestratorAction);
    }

    public void testShouldReturnPhaseHandler() {
        Action returnAction = null;
        try {
            returnAction = factory.getAction(TestStage.class.getMethod("executeCreate", TargetObject.class));
        } catch (NoSuchMethodException e) {
            throw new RuntimeException(e.getMessage());
        }
        assertTrue(returnAction instanceof PhaseAction);
    }
}