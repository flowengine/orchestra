package flowengine.orchestra.stage;

import TestActors.SimpleTestActor;
import TestActors.TargetObject;
import TestStages.TestStage;
import flowengine.orchestra.orchestrator.Orchestrator;
import junit.framework.TestCase;

/**
 * Created by Ignacio on 22/08/2016.
 */
public class StageProxyTest extends TestCase {

    private StageFactory stageFactory;
    private StageActionFactory stageActionFactory;
    private TestStage stage;

    public void setUp() throws Exception {
        super.setUp();
        stageActionFactory = new StageActionFactoryImplementor();
        stageFactory = new StageFactoryImplementor(stageActionFactory);
        stage = stageFactory.getStage(TestStage.class);
    }

    public void testExecutePhase() throws Exception {
        TargetObject obj = new TargetObject();
        Orchestrator o1 = createOrchestrator(1);
        Orchestrator o2 = createOrchestrator(2);
        stage.setOrchestratorCreate(o1);
        stage.setOrchestratorStop(o2);
        stage.executeCreate(obj);
        assertEquals(1, obj.getElements().size());
        assertEquals(new Integer(1), obj.getElements().get(0));
        stage.executeStop(obj);
        assertEquals(2, obj.getElements().size());
        assertEquals(new Integer(2), obj.getElements().get(1));
        assertTrue("2 comes after 1", obj.comesBefore(1, 2));
    }

    private Orchestrator createOrchestrator(Integer i) {
        Orchestrator o1 = new Orchestrator();
        o1.registerActorForClass(TargetObject.class, new SimpleTestActor(i));
        o1.buildExecutionTrees();
        return o1;
    }
}