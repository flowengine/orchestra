package flowengine.orchestra.annotations;

import flowengine.orchestra.orchestrator.Actor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Ignacio on 20/08/2016.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DependsOn {
    Class<? extends Actor<?>>[] actors();
}
