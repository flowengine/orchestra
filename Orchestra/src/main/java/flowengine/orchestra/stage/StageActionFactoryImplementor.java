package flowengine.orchestra.stage;

import flowengine.orchestra.annotations.Phase;
import flowengine.orchestra.annotations.SetOrchestrator;
import flowengine.orchestra.stage.actions.Action;
import flowengine.orchestra.stage.actions.PhaseAction;
import flowengine.orchestra.stage.actions.SetOrchestratorAction;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ignacio on 21/08/2016.
 */

public class StageActionFactoryImplementor implements StageActionFactory {

    private Map<Class<? extends Annotation>, Action> actionHandlers;

    public StageActionFactoryImplementor() {
        actionHandlers = new HashMap();
        actionHandlers.put(SetOrchestrator.class, new SetOrchestratorAction());
        actionHandlers.put(Phase.class, new PhaseAction());
    }

    public void registerActionHandler(Class<? extends Annotation> annotation, Action handler) {
        if (annotation.getAnnotation(flowengine.orchestra.annotations.Action.class) == null) {
            throw new RuntimeException("Cannot register annotation " + annotation.getCanonicalName() +
                    " because it lacks Action annotation");
        }
        actionHandlers.put(annotation, handler);
    }

    public Action getAction(Method method) {
        Annotation[] annotations = method.getDeclaredAnnotations();
        validateSingleAction(annotations);
        Class<? extends Annotation> annotationType = annotations[0].annotationType();
        if (actionHandlers.containsKey(annotationType)) {
            return actionHandlers.get(annotationType);
        }
        throw new RuntimeException("No action handler for stage method annotation " + annotationType.getCanonicalName());
    }

    private void validateSingleAction(Annotation[] annotations) {
        if (annotations == null || annotations.length == 0) {
            throw new RuntimeException("Stage method has no annotated behaviour");
        }
        int i = 0;
        for (Annotation annotation : annotations) {
            if (annotation instanceof Action) {
                i++;
            }
            if (i > 1) {
                throw new RuntimeException("Stage method has more than one action associated annotation");
            }
        }
    }
}
