package flowengine.orchestra.stage.actions;

import flowengine.orchestra.annotations.SetOrchestrator;
import flowengine.orchestra.orchestrator.Orchestrator;
import flowengine.orchestra.stage.StageHandler;

import java.lang.reflect.Method;

/**
 * Created by Ignacio on 21/08/2016.
 */
public class SetOrchestratorAction extends ActionHandler {

    @Override
    protected void executeAction(Object proxy, Method method, Object[] args, StageHandler handler) {
        SetOrchestrator annotation = method.getAnnotation(SetOrchestrator.class);
        handler.setOrchestratorForPhase(annotation.phase(), (Orchestrator) args[0]);
    }

    @Override
    protected void validateMethodArguments(Object proxy, Method method, Object[] args) throws Throwable {
        SetOrchestrator annotation = method.getAnnotation(SetOrchestrator.class);
        if (annotation.phase() == null || annotation.phase().isEmpty()) {
            throw new RuntimeException("Phase for SetOrchestrator not set");
        }
        if (args == null || args.length != 1) {
            throw new RuntimeException("Method arguments for SetOrchestrator should be a single Orchestrator");
        }
        Object arg = args[0];
        if (!(arg instanceof Orchestrator)) {
            throw new RuntimeException("Method argument for SetOrchestrator not an Orchestrator");
        }
        if (arg == null) {
            throw new RuntimeException("Orchestrator parameter cannot be null");
        }
    }
}
