package flowengine.orchestra.stage.actions;

import flowengine.orchestra.stage.StageHandler;

import java.lang.reflect.Method;

/**
 * Created by Ignacio on 21/08/2016.
 */
public abstract class ActionHandler implements Action {

    public void invoke(Object proxy, Method method, Object[] args, StageHandler handler) throws Throwable {
        executeAction(proxy, method, args, handler);
    }

    protected abstract void executeAction(Object proxy, Method method, Object[] args, StageHandler handler);

    protected abstract void validateMethodArguments(Object proxy, Method method, Object[] args) throws Throwable;
}
