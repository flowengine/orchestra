package flowengine.orchestra.stage.actions;

import flowengine.orchestra.annotations.Phase;
import flowengine.orchestra.annotations.Stage;
import flowengine.orchestra.stage.StageHandler;

import java.lang.reflect.Method;

/**
 * Created by Ignacio on 21/08/2016.
 */
public class PhaseAction extends ActionHandler {

    @Override
    protected void executeAction(Object proxy, Method method, Object[] args, StageHandler handler) {
        Phase phaseAnnotation = method.getAnnotation(Phase.class);
        handler.executePhase(phaseAnnotation.name(), args[0], proxy);
    }

    @Override
    protected void validateMethodArguments(Object proxy, Method method, Object[] args) {
        Phase annotation = method.getAnnotation(Phase.class);
        if (annotation == null) {
            throw new RuntimeException("Method not annotated with Phase");
        }
        if (annotation.name() == null || annotation.name().isEmpty()) {
            throw new RuntimeException("Phase name not set");
        }
        if (args == null || args.length != 1) {
            throw new RuntimeException("Method arguments for Phase should be a single element");
        }
        Object arg = args[0];
        validateArgumentType(proxy.getClass(), arg.getClass());
    }


    private void validateArgumentType(Class<?> proxy, Class<?> arg) {
        for (Class<?> appliedInterface : proxy.getClass().getInterfaces()) {
            Stage annotation = appliedInterface.getAnnotation(Stage.class);
            if (annotation == null) {
                continue;
            }
            if (annotation.forClass() == null) {
                throw new RuntimeException("Stage needs a class as a target");
            }
            return;
        }
        throw new RuntimeException("Stage inteface not annotated with Stage");
    }
}
