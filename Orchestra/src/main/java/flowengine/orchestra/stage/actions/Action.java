package flowengine.orchestra.stage.actions;

import flowengine.orchestra.stage.StageHandler;

import java.lang.reflect.Method;

/**
 * Created by Ignacio on 21/08/2016.
 */
public interface Action {
    void invoke(Object proxy, Method method, Object[] args, StageHandler handler) throws Throwable;
}
