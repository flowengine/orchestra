package flowengine.orchestra.stage;

import flowengine.orchestra.annotations.Stage;
import flowengine.orchestra.orchestrator.Orchestrator;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ignacio on 21/08/2016.
 */
public class StageHandler implements InvocationHandler {

    private Map<String, Orchestrator> phasesMap;

    private Class<?> interfaceClass;

    private String id;

    private StageActionFactory stageActionFactory;

    public <T> StageHandler(StageActionFactory stageActionFactory, Class<T> stageInterface, String id) {
        phasesMap = new HashMap();
        interfaceClass = stageInterface;
        this.stageActionFactory = stageActionFactory;
        this.id = id;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (!method.getReturnType().equals(Void.TYPE)) {
            throw new RuntimeException("Return types are not allowed for flowengine.orchestra.stage methods");
        }
        stageActionFactory.getAction(method).invoke(proxy, method, args, this);
        return null;
    }

    public void setOrchestratorForPhase(String phase, Orchestrator orchestrator) {
        phasesMap.put(phase, orchestrator);
    }

    public void executePhase(String phase, Object arg, Object proxy) {
        if (!phasesMap.containsKey(phase)) {
            throw new RuntimeException("There is no registered orchestrator for phase " + phase);
        }
        validateArgumentType(proxy, arg.getClass());
        Stage stage = interfaceClass.getAnnotation(Stage.class);
        phasesMap.get(phase).execute(stage.forClass().cast(arg));
    }

    private void validateArgumentType(Object proxy, Class<?> arg) {
        if (arg == null)
            return;
        Stage annotation = interfaceClass.getAnnotation(Stage.class);
        if (annotation == null) {
            throw new RuntimeException("Interface not annotated with Stage");
        }
        if (annotation.forClass() == null) {
            throw new RuntimeException("Stage needs a class as a target");
        }
    }
}
