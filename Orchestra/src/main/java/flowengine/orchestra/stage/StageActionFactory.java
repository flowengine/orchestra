package flowengine.orchestra.stage;

import flowengine.orchestra.stage.actions.Action;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Created by Ignacio on 21/08/2016.
 */

public interface StageActionFactory {

    void registerActionHandler(Class<? extends Annotation> annotation, Action handler);

    Action getAction(Method method);
}
