package flowengine.orchestra.stage;

/**
 * Created by Ignacio on 21/08/2016.
 */
public interface StageFactory {
    <T> T getStage(Class<T> stageInterface);
}
