package flowengine.orchestra.stage;

import java.lang.reflect.Proxy;
import java.util.UUID;

/**
 * Created by Ignacio on 21/08/2016.
 */
public class StageFactoryImplementor implements StageFactory {

    private StageActionFactory stageActionFactory;

    public StageFactoryImplementor(StageActionFactory stageActionFactory) {
        this.stageActionFactory = stageActionFactory;
    }

    public <T> T getStage(Class<T> stageInterface) {
        return (T) Proxy.newProxyInstance(
                stageInterface.getClassLoader(),
                new Class[]{stageInterface},
                new StageHandler(stageActionFactory, stageInterface, UUID.randomUUID().toString())
        );
    }
}
