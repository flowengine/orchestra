package flowengine.orchestra.orchestrator;

import flowengine.orchestra.annotations.DependsOn;

import java.util.*;

/**
 * Created by Ignacio on 20/08/2016.
 */
public class ExecutionPlan {

    private List<Actor> actors;

    private Map<Class<?>, DependsOn> annotationCache;

    private class PriorizedActor implements Comparable<PriorizedActor> {

        public Actor actor;
        public Integer neighbourCount;

        public PriorizedActor(Integer neigbourCount, Actor actor) {
            this.neighbourCount = neigbourCount;
            this.actor = actor;
        }

        public int compareTo(PriorizedActor o) {
            return neighbourCount - o.neighbourCount;
        }
    }

    public <T> ExecutionPlan(Set<Actor> unorderedActors) {
        annotationCache = new HashMap();
        actors = new ArrayList();
        PriorityQueue<PriorizedActor> orderedActors = buildPriorizedActors(unorderedActors);
        while(!orderedActors.isEmpty()) {
            PriorizedActor priorizedActor = orderedActors.poll();
            actors.add(priorizedActor.actor);
        }
    }

    private PriorityQueue<PriorizedActor> buildPriorizedActors(Set<Actor> unorderedActors) {
        PriorityQueue<PriorizedActor> dependencies = new PriorityQueue();
        Set<Class<?>> actorClasses = getActorsClasses(unorderedActors);
        for (Actor actor : unorderedActors) {
            checkActorConsistency(actorClasses, actor.getClass());
            dependencies.add(new PriorizedActor(countDependencies(actor), actor));
        }
        return dependencies;
    }

    private void checkActorConsistency(Set<Class<?>> actorClasses, Class<?> actor) {
        if (!dependenciesMet(actor, actorClasses)) {
            throw new RuntimeException("Actor has more dependencies than provided");
        }
        if (hasCircularDependencies(actor)) {
            throw new RuntimeException("The dependency graph given has cycles");
        }
    }

    private Set<Class<?>> getActorsClasses(Set<Actor> unorderedActors) {
        Set<Class<?>> actorClasses = new HashSet();
        for (Actor actor : unorderedActors) {
            actorClasses.add(actor.getClass());
        }
        return actorClasses;
    }

    private Integer countDependencies(Actor actor) {
        Map<Class<? extends Actor>, Integer> map = new HashMap();
        map.put(actor.getClass(), 0);
        return countDependencies(actor.getClass(), 0, map);
    }

    private Integer countDependencies(Class<? extends Actor> actorClass, int previous, Map<Class<? extends Actor>, Integer> visited) {
        DependsOn dependsOn = getDependsOnAnnotationFromActor(actorClass);
        if (dependsOn == null || dependsOn.actors() == null || dependsOn.actors().length == 0) {
            return previous;
        }
        for (Class<? extends Actor> dependency : dependsOn.actors()) {
            if (!visited.containsKey(dependency)) {
                visited.put(dependency, countDependencies(dependency, 0, visited));
            }
            previous += 1 + visited.get(dependency);
        }
        return previous;
    }

    /* TODO: add furhter circular dependency validation, right now it only has
    first level. The algorithm should be a graph cycle detection algoritm */
    private boolean hasCircularDependencies(Class<?> actor) {
        DependsOn annotation = getDependsOnAnnotationFromActor(actor);
        if (!annotationHasDependencies(annotation)) {
            return false;
        }
        for (Class<?> dependency : annotation.actors()) {
            if (hasClassDependency(dependency, actor))
                return true;
        }
        return false;
    }

    private boolean hasClassDependency(Class<?> rootClass, Class<?> testedDependency) {
        DependsOn annotation = getDependsOnAnnotationFromActor(rootClass);
        if (!annotationHasDependencies(annotation)) {
            return false;
        }
        for (Class<?> dependency : annotation.actors()) {
            if (dependency == testedDependency) {
                return true;
            }
        }
        return false;
    }

    private boolean dependenciesMet(Class<?> actor, Set<Class<?>> actorClasses) {
        DependsOn annotation = getDependsOnAnnotationFromActor(actor);
        if (!annotationHasDependencies(annotation)) {
            return true;
        }
        for(Class<?> dependency : annotation.actors()) {
            if (!actorClasses.contains(dependency)) {
                return false;
            }
        }
        return true;
    }

    private boolean annotationHasDependencies(DependsOn annotation) {
        return annotation != null && annotation.actors() != null && annotation.actors().length == 0;
    }

    private DependsOn getDependsOnAnnotationFromActor(Class<?> annotatedClass) {
        if (!annotationCache.containsKey(annotatedClass)) {
            annotationCache.put(annotatedClass, annotatedClass.getAnnotation(DependsOn.class));
        }
        return annotationCache.get(annotatedClass);
    }

    public <T> void execute(T target) {
        for(Actor actor : actors) {
            actor.execute(target);
        }
    }
}
