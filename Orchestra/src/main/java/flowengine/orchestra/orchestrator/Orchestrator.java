package flowengine.orchestra.orchestrator;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Created by Ignacio on 20/08/2016.
 */
public class Orchestrator {

    public Orchestrator() {
        this.state = OrchestratorStates.BUILDING;
        actorsPerClass = new HashMap();
        executionPlanPerClass = new HashMap();
    }

    public <T> void registerActorForClass(Class<T> targetClass, Actor actor) {
        checkValidRegistrationState();
        checkTargetClassCompatibleWithActor(targetClass, actor);
        if (!actorsPerClass.containsKey(targetClass)) {
            actorsPerClass.put(targetClass, new HashSet());
        }
        Set<Actor> actors = actorsPerClass.get(targetClass);
        if (!actors.contains(actor)) {
            actors.add(actor);
        }
    }

    private void checkTargetClassCompatibleWithActor(Class<?> targetClass, Actor actor) {
        //TODO check if actor is of generic targetClass
    }

    public <T> void buildExecutionTrees() {
        finishBuildingPhase();
        for(Class<?> targetClass : actorsPerClass.keySet()) {
            executionPlanPerClass.put(targetClass, new ExecutionPlan(actorsPerClass.get(targetClass)));
        }
    }

    public <T> void execute(T param) {
        if (state != OrchestratorStates.OPERATIONAL)
            throw new RuntimeException("Still at building phase. Need to call BuildExecitionTrees");
        if (!executionPlanPerClass.containsKey(param.getClass())) {
            throw new RuntimeException("No execution plan for class");
        }
        executionPlanPerClass.get(param.getClass()).execute(param);
    }

    private void finishBuildingPhase() {
        if (state != OrchestratorStates.BUILDING) {
            throw new RuntimeException("Cannot finish building phase more than once");
        }
        state = OrchestratorStates.OPERATIONAL;
    }

    private void checkValidRegistrationState() {
        if (state != OrchestratorStates.BUILDING) {
            throw new RuntimeException("Cannot register actors after building phase is over");
        }
    }

    private OrchestratorStates state;
    private Map<Class<?>, Set<Actor>> actorsPerClass;
    private Map<Class<?>, ExecutionPlan> executionPlanPerClass;


    private enum OrchestratorStates {
        BUILDING, OPERATIONAL
    }

}
