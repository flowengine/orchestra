package flowengine.orchestra.orchestrator;

/**
 * Created by Ignacio on 20/08/2016.
 */
public interface  Actor<T> {
    void execute(T target);
}
